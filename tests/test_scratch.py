import unittest
from problems.scratch import scratch


class scratchTestCase(unittest.TestCase):
    def test_1(self):
        self.assertEqual(scratch(3, 5), 8)

    def test_2(self):
        self.assertEqual(scratch(5, 15), 20)

    def test_all_positive(self):
        self.assertEqual(scratch(9, 9), 18)
        self.assertEqual(scratch(35, 5), 40)
        self.assertEqual(scratch(20, 30), 50)

    def test_all_negative(self):
        self.assertEqual(scratch(-9, -9), -18)
        self.assertEqual(scratch(-35, -5), -40)
        self.assertEqual(scratch(-20, -30), -50)

    def test_positive_and_negative(self):
        self.assertEqual(scratch(-9, 9), 0)
        self.assertEqual(scratch(35, -5), 30)
        self.assertEqual(scratch(20, -30), -10)

    def test_should_fail(self):
        self.assertEqual(scratch(5, 15), 21)


if __name__ == "__main__":
    unittest.main()
